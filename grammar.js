const PREC = {
  PAREN_DECLARATOR: -10,
  MID : 1,
  BEFORE: 2,
  THEN: 3,
  IN: 3,
  ELSE: 4,
  SEMI: 5,
  OR:6,
  WEDGE:7,
  EQUAL:8,
  DIFF:9,
  LEQ:10,
  GEQ:11,
  GREATER:12,
  LESS:13,
  PLUS: 14,
  MINUS: 14,
  REPL: 15
};

module.exports = grammar({
  name: 'proverif',

  words: $ => $.ident,

  extras: $ => [
    /\s/,
    $.comment,
  ],

  rules: {

    // Starting rule
    source_file: $ => choice(
      seq(repeat($._lib),optional(seq('process',$.tprocess))),
      seq(repeat($._lib),optional(seq('equivalence',$.tprocess,$.tprocess)))
    ),

    // Main declarations

    type_declaration: $ => seq('type',$.ident,optional($.options),'.'),
    fun_declaration: $ => seq('fun',$.ident,'(',$._typeid_seq,')',':',$.typeid, optional(seq('reduc',$.treduc_mayfail)),optional($.options),'.'),
    reduc_declaration: $ => seq('reduc',$._treduc_seq,optional($.options),'.'),
    const_declaration: $ => seq('const',$._ident_seq,':',$.typeid,optional($.options),'.'),
    equation_declaration: $ => seq('equation',$._equation_seq,optional($.options),'.'),
    event_declaration: $ => seq('event',$.ident,optional(seq('(',$._typeid_seq,')')),'.'),
    predicate_declaration: $ => seq('pred',$.ident,optional(seq('(',$._typeid_seq,')')),optional($.options),'.'),
    table_declaration: $ => seq('table',$.ident,'(',$._typeid_seq,')','.'),
    process_declaration: $ => seq('let',$.ident,optional(seq('(',$._mayfail_variables_type_seq,')')),'=',$.tprocess,'.'),
    letfun_declaration: $ => seq('letfun',$.ident,optional(seq('(',$._mayfail_variables_type_seq,')')),'=',$.pterm,'.'),
    setting_declaration: $ => seq('set',$.ident,'=',choice($.ident,$.string,$.int),'.'),
    nounif_declaration: $ => seq('nounif',optional(seq($._variables_type_seq,';')),$.nounif,optional($.options),'.'),
    query_declaration: $ => seq('query',optional(seq($._variables_type_seq,';')),$._query_seq,optional($.options),'.'),
    noninterf_declaration: $ => seq('noninterf',optional(seq($._variables_type_seq,';')),$._noninterf_seq,'.'),
    weaksecret_declaration: $ => seq('weaksecret',$.ident,'.'),
    not_declaration: $ => seq('not',optional(seq($._variables_type_seq,';')),$.qterm,'.'),
    param_declaration: $ => seq('param',$._ident_seq,optional($.options),'.'),
    proba_declaration: $ => seq('proba',$.ident,'.'),
    proof_declaration: $ => seq('proof','{',$._proof_seq,'}'),
    implementation_declaration: $ => seq('implementation',$._impl_seq,'.'),
    elim_true_declaration: $ => seq('elimtrue',optional(seq($._mayfail_variables_type_seq,';')),$.term,'.'),
    channel_declaration: $ => seq('channel',$._ident_seq,'.'),
    free_declaration: $ => seq('free',$._ident_seq,':',$.typeid,optional($.options),'.'),
    clauses_declaration: $ => seq('clauses',$._clause_seq,'.'),
    define_declaration: $ => seq('def',$.ident,'(',$._typeid_seq,')','{',$._lib,'}'),
    expand_declaration: $ => seq('expand',$.ident,'(',$._typeid_seq,')','.'),
    lemma_declaration: $ => seq($.lemma_type,optional(seq($._variables_type_seq,';')),$._lemma_seq,optional($.options),'.'),

    _lib: $ => choice(
      $.type_declaration,
      $.fun_declaration,
      $.reduc_declaration,
      $.const_declaration,
      $.equation_declaration,
      $.event_declaration,
      $.predicate_declaration,
      $.table_declaration,
      $.process_declaration,
      $.letfun_declaration,
      $.setting_declaration,
      $.nounif_declaration,
      $.query_declaration,
      $.noninterf_declaration,
      $.weaksecret_declaration,
      $.not_declaration,
      $.param_declaration,
      $.proba_declaration,
      $.proof_declaration,
      $.implementation_declaration,
      $.elim_true_declaration,
      $.channel_declaration,
      $.free_declaration,
      $.clauses_declaration,
      $.define_declaration,
      $.expand_declaration,
      $.lemma_declaration
    ),

    // Options

    option: $ => choice(
      $.ident,
      seq($.ident,'=',$.ident),
      seq($.ident,'=','{',$._ident_seq,'}')
    ),
    _option_seq: $ => prec.right(seq($.option,optional(seq(',',$._option_seq)))),

    options: $ => seq('[',$._option_seq,']'),

    // Types

    typeid: $ => $.ident,
    _typeid_seq: $ => prec.right(seq($.typeid,optional(seq(',',$._typeid_seq)))),

    // Variables declarations

    variables_type: $ => seq($._ident_seq,':',$.typeid),
    _variables_type_seq: $ => prec.right(seq($.variables_type,optional(seq(',',$._variables_type_seq)))),

    mayfail_variables_type: $ => seq($._ident_seq,':',$.typeid,optional(seq('or','fail'))),
    _mayfail_variables_type_seq: $ => prec.right(seq($.mayfail_variables_type,optional(seq(',',$._mayfail_variables_type_seq)))),

    // Rewrite rules

    forall_variables_type: $ => seq('forall',$._variables_type_seq,';'),

    forall_mayfail_variables_type: $ => seq('forall',$._mayfail_variables_type_seq,';'),

    treduc_otherwise: $ => repeat1(seq('otherwise',$.forall_mayfail_variables_type,$.term,'=',$.term)),

    treduc_mayfail: $ => seq($.forall_mayfail_variables_type,$.term,'=',$.term,optional($.treduc_otherwise)),

    treduc: $ => seq($.forall_variables_type,$.term,'=',$.term),
    _treduc_seq: $ => prec.right(seq($.treduc,optional(seq(';',$._treduc_seq)))),

    // Equations

    equation: $ => seq($.forall_variables_type,$.term),
    _equation_seq: $ => prec.right(seq($.equation,optional(seq(';',$._equation_seq)))),

    // Clauses

    clause: $ => seq($.forall_mayfail_variables_type, choice(
      seq($.term,choice('->','<->','<=>'),$.term),
      $.term,
    )),
    _clause_seq: $ => prec.right(seq($.clause,optional(seq(';',$._clause_seq)))),

    // Phase

    phase: $ => seq('phase',$.int),

    // Nounif

    nounif: $ => choice(
      seq('let',alias($.ident,$.var_gen),'=',$.gformat,'in',$.nounif),
      seq($.ident,'(',$._gformat_seq,')',optional($.phase),optional(seq('/',$.int))),
      seq($.ident,optional(seq('/',$.int))),
      seq('table','(',$._gformat_seq,')',optional($.phase),optional(seq('/',$.int))),
      seq('event','(',$._gformat_seq,')',optional($.phase),optional(seq('/',$.int))),
    ),

    // Non-interference

    // noninterf: $ => seq($.ident,optional(seq('among','(',$._term_seq,')'))),
    // _noninterf_seq : $ => seq($.noninterf,optional(seq(',',$._noninterf_seq))),

    _noninterf_seq : $ => choice(
      seq($.ident,'among','(',$._term_seq,')',',',$._noninterf_seq),
      seq($.ident,'among','(',$._term_seq,')'),
      seq($.ident,',',$._noninterf_seq),
      seq($.ident)
    ),

    // Proofs

    proof_token: $ => choice($.ident,$.string,$.int,'*','.','set','insert','=',',','(',')'),
    proof: $ => repeat1($.proof_token),
    _proof_seq: $ => prec.right(seq($.proof,optional(seq(';',$._proof_seq)))),

    // Implementation annotations

    cvtypeid: $ => choice($.int,$.string),

    typeopt: $ => choice(
      seq($.ident,'=',$._string_seq),
      seq('pred','=',$._string_seq)
    ),
    _typeopt_seq: $ => prec.right(seq($.typeopt,optional(seq(';',$._typeopt_seq)))),
    type_options: $ => seq('[',$._typeopt_seq,']'),

    funopt : $ => seq($.ident,'=',$.string),
    _funopt_seq: $ => prec.right(seq($.funopt,optional(seq(';',$._funopt_seq)))),
    fun_options: $ => seq('[',$._funopt_seq,']'),

    impl: $ => choice(
      seq('type',$.ident,'=',$.cvtypeid,optional($.type_options)),
      seq('fun',$.ident,'=',$.string,optional($.type_options)),
      seq('table',$.ident,'=',$.string),
      seq('const',$.ident,'=',$.string)
    ),
    _impl_seq: $ => prec.right(seq($.impl,optional(seq(';',$._impl_seq)))),

    // Format

    format_binding: $ => choice(
      seq('!',$.int,'=',$.gformat),
      seq($.ident,'=',$.gformat)
    ),
    _format_binding_seq: $ => prec.right(seq($.format_binding,optional($._format_binding_seq))),

    gformat: $ => choice(
      // Tuples
      seq('(',optional($._gformat_seq),')'),
      // Function application
      seq($.ident,'(',$._gformat_seq,')'),
      seq(choice('choice','diff'),'[',$.gformat,',',$.gformat,']'),
      $.ident,
      // Natural numbers
      $.int,
      prec.left(9,seq($.int,'+',$.gformat)),
      prec.left(9,seq($.gformat,'+',$.int)),
      // Binding
      seq('new',alias($.ident,$.name_gen),optional(seq('[',optional($._format_binding_seq),']'))),
      seq('*',$.ident),
      seq('let',alias($.ident,$.var_gen),'=',$.gformat,'in',$.gformat)
    ),
    _gformat_seq: $ => prec.right(seq($.gformat,optional(seq(',',$._gformat_seq)))),

    // Terms

    term: $ => choice(
      // Failure
      'fail',
      // Tuples
      seq('(',optional($._pterm_seq),')'),
      // Function application
      seq($.ident,'(',$._term_seq,')'),
      seq($.projection,'(',$.term,')'),
      seq(choice('choice','diff'),'[',$.term,',',$.term,']'),
      $.ident,
      // Natural numbers
      $.int,
      prec.left(9,seq($.term,'-',$.int)),
      prec.left(9,seq($.int,'+',$.term)),
      prec.left(9,seq($.term,'+',$.int)),
      // Operators on terms
      prec.right(1,seq($.term,'||',$.term)),
      prec.right(2,seq($.term,'&&',$.term)),
      prec.left(3,seq($.term,'=',$.term)),
      prec.left(4,seq($.term,'<>',$.term)),
      prec.left(5,seq($.term,'<=',$.term)),
      prec.left(6,seq($.term,'>=',$.term)),
      prec.left(7,seq($.term,'>',$.term)),
      prec.left(8,seq($.term,'<',$.term)),
      seq('not','(',$.term,')'),
    ),
    _term_seq: $ => prec.right(seq($.term,optional(seq(',',$._term_seq)))),

    // Query term

    at_ident: $ => seq('@',$.ident),

    binding: $ => choice(
      seq('!',$.int,'=',$.qterm),
      seq($.ident,'=',$.qterm)
    ),
    _binding_seq: $ => prec.right(seq($.binding,optional($._binding_seq))),

    qterm: $ => choice(
      seq($.ident,'(',$._qterm_seq,')',optional($.phase),optional($.at_ident)),
      $.ident,
      // Tuples
      seq('(',optional($._qterm_seq),')'),
      // Natural numbers
      $.int,
      prec.left(9,seq($.int,'+',$.qterm)),
      prec.left(9,seq($.qterm,'+',$.int)),
      // Operators on terms
      prec.right(1,seq($.qterm,'||',$.qterm)),
      prec.right(2,seq($.qterm,'&&',$.qterm)),
      prec.left(3,seq($.qterm,'=',$.qterm)),
      prec.left(4,seq($.qterm,'<>',$.qterm)),
      prec.left(5,seq($.qterm,'<=',$.qterm)),
      prec.left(6,seq($.qterm,'>=',$.qterm)),
      prec.left(7,seq($.qterm,'>',$.qterm)),
      prec.left(8,seq($.qterm,'<',$.qterm)),
      seq('not','(',$.qterm,')'),
      seq('table','(',$._qterm_seq,')',optional($.phase),optional($.at_ident)),
      seq('event','(',$._qterm_seq,')',optional($.at_ident)),
      seq('inj-event','(',$._qterm_seq,')',optional($.at_ident)),
      seq(choice('choice','diff'),'[',$.qterm,',',$.qterm,']'),
      prec.left(1,seq($.qterm,'==>',$.qterm)),
      // Binding
      prec.left(2,seq('new',alias($.ident, $.name_gen),optional(seq('[',optional($._binding_seq),']')))),
      seq('let',alias($.ident, $.var_gen),'=',$.qterm,'in',$.qterm)
    ),
    _qterm_seq: $ => prec.right(seq($.qterm,optional(seq(',',$._qterm_seq)))),

    // Patterns

    basic_pattern: $ => seq($.ident,optional(seq(':',$.typeid))),

    tpattern: $ => choice(
      $.basic_pattern,
      seq('(',$._tpattern_seq,')'),
      seq($.ident,'(',$._tpattern_seq,')'),
      seq(choice('diff','choice'),'[',$.tpattern,',',$.tpattern,']',optional(seq(':',$.typeid))),
      $.int,
      prec.left(PREC.PLUS,seq($.tpattern,'+',$.int)),
      prec.left(PREC.PLUS,seq($.int,'+',$.tpattern)),
      seq('=',$.pterm)
    ),

    _tpattern_seq: $ => prec.right(seq($.tpattern,optional(seq(',',$._tpattern_seq)))),

    // Process term

    pterm: $ => prec.left(choice(
      // Tuples
      seq('(',')'),
      prec(PREC.PAREN_DECLARATOR,seq('(',$.pterm,')')),
      seq('(',$._pterm_seq,')'),
      // Function application
      seq($.ident,'(',optional($._pterm_seq),')'),
      // Choice application
      seq(choice('diff','choice'),'[',$.pterm,',',$.pterm,']'),
      // Identifiers
      $.ident,
      // Natural numbers
      $.int,
      prec.left(PREC.MINUS,seq($.pterm,'-',$.int)),
      prec.left(PREC.PLUS,seq($.int,'+',$.pterm)),
      prec.left(PREC.PLUS,seq($.pterm,'+',$.int)),
      // Operators on terms
      prec.right(PREC.OR,seq($.pterm,'||',$.pterm)),
      prec.right(PREC.WEDGE,seq($.pterm,'&&',$.pterm)),
      prec.left(PREC.EQUAL,seq($.pterm,'=',$.pterm)),
      prec.left(PREC.DIFF,seq($.pterm,'<>',$.pterm)),
      prec.left(PREC.LEQ,seq($.pterm,'<=',$.pterm)),
      prec.left(PREC.GEQ,seq($.pterm,'>=',$.pterm)),
      prec.left(PREC.GREATER,seq($.pterm,'>',$.pterm)),
      prec.left(PREC.LESS,seq($.pterm,'<',$.pterm)),
      seq('not','(',$.pterm,')'),
      // Extended terms
      seq('new',alias($.ident,$.name_gen),optional($.newarg),':',$.typeid,';',$.pterm),
      seq(alias($.ident,$.name_gen),'<-R',$.typeid,';',$.pterm),
      prec(PREC.ELSE,seq('if',$.pterm,'then',$.pterm,'else',$.pterm)),
      prec(PREC.THEN,seq('if',$.pterm,'then',$.pterm)),
      prec(PREC.IN,seq('let',$.tpattern,'=',$.pterm,'in',$.pterm)),
      prec(PREC.ELSE,seq('let',$.tpattern,'=',$.pterm,'in',$.pterm,'else',$.pterm)),
      seq($.basic_pattern,'<-',$.pterm,';',$.pterm),
      prec(PREC.IN,seq('let',$._variables_type_seq,'suchthat',$.pterm,'in',$.pterm)),
      prec(PREC.ELSE,seq('let',$._variables_type_seq,'suchthat',$.pterm,'in',$.pterm,'else',$.pterm)),
      seq('event',alias($.ident,$.fun_call),optional(seq('(',$._pterm_seq,')')),optional($.newarg),';',$.pterm),
      seq('insert',alias($.ident,$.fun_call),'(',$._pterm_seq,')',';',$.pterm),
      prec(PREC.IN,seq('get',alias($.ident,$.fun_call),'(',$._tpattern_seq,')',optional(seq('suchthat',$.pterm)),optional($.options),'in',$.pterm)),
      prec(PREC.ELSE,seq('get',alias($.ident,$.fun_call),'(',$._tpattern_seq,')',optional(seq('suchthat',$.pterm)),optional($.options),'in',$.pterm,'else',$.pterm))
    )),

    _pterm_seq: $ => prec.right(seq($.pterm,optional(seq(',',$._pterm_seq)))),

    // Queries

    public_vars_ror: $ => choice(
      seq('for','{','public_vars',$._ident_seq,'}'),
      seq('for','{','secret',$.ident,optional($.public_vars_ror),'[',$.ident,']','}'),
    ),

    query: $ => choice(
      seq($.qterm,optional(seq('public_vars',$._ident_seq))),
      prec.left(seq('secret',$.ident,optional(seq('public_vars',$._ident_seq)),optional($.options))),
      seq('putbegin','event',':',$._ident_seq),
      seq('putbegin','inj-event',':',$._ident_seq)
    ),
    _query_seq: $ => prec.right(seq($.query,optional(seq(';',$._query_seq)))),

    // Lemmas

    lemma_type: $ => choice('axiom','lemma','restriction'),
    lemma: $ => seq($.qterm,optional($.public_vars_ror)),
    _lemma_seq: $ => prec.right(seq($.lemma,optional(seq(';',$._lemma_seq)))),

    // Process

    program_opt: $ => seq($.ident,choice('<','>'),$.ident),
    _program_opt_seq: $ => prec.right(seq($.program_opt,optional(seq(',',$._program_opt_seq)))),
    program_options: $ => seq('[',$._program_opt_seq,']'),
    program_begin: $ => seq($.ident,optional($.program_options),'{'),
    program_end: $ => '}',

    sync_option: $ => seq('[','sync',':',$.ident,$.ident,$.ident,']'),

    newarg: $ => seq('[',optional($._ident_seq),']'),

    tprocess: $ => prec.left(choice(
      seq($.program_begin,$.tprocess),
      prec(PREC.PAREN_DECLARATOR,seq('(',$.tprocess,')')),
      seq($.ident,optional(seq('(',$._pterm_seq,')')),optional($.sync_option)),
      prec(PREC.REPL,seq('!',optional(seq($.ident,'>=',$.ident)),$.tprocess)),
      prec(PREC.REPL,seq('foreach',$.ident,'>=',$.ident,'do',$.tprocess)),
      '0',
      'yield',
      seq('new',alias($.ident,$.name_gen),optional($.newarg),':',$.typeid,optional(seq(';',$.tprocess))),
      seq(alias($.ident,$.name_gen),'<-R',$.typeid,optional(seq(';',$.tprocess))),
      prec(PREC.ELSE,seq('if',$.pterm,'then',$.tprocess,'else',$.tprocess)),
      prec(PREC.THEN,seq('if',$.pterm,'then',$.tprocess)),
      seq('in','(',$.pterm,',',$.tpattern,')',optional($.options),optional(seq(';',$.tprocess))),
      seq('out','(',$.pterm,',',$.pterm,')',optional($.program_end),optional(seq(';',$.tprocess))),

      seq('let',$.tpattern,'=',$.pterm),
      prec(PREC.IN,seq('let',$.tpattern,'=',$.pterm,'in',$.tprocess)),
      prec(PREC.ELSE,seq('let',$.tpattern,'=',$.pterm,'in',$.tprocess,'else',$.tprocess)),

      seq($.basic_pattern,'<-',$.pterm),
      prec(PREC.SEMI,seq($.basic_pattern,'<-',$.pterm,';',$.tprocess)),

      seq('let',$._variables_type_seq,'suchthat',$.pterm,optional($.options)),
      prec(PREC.IN,seq('let',$._variables_type_seq,'suchthat',$.pterm,optional($.options),'in',$.tprocess)),
      prec(PREC.ELSE,seq('let',$._variables_type_seq,'suchthat',$.pterm,optional($.options),'in',$.tprocess,'else',$.tprocess)),

      seq('event',alias($.ident,$.fun_call),optional(seq('(',$._pterm_seq,')')),optional($.newarg),optional(seq(';',$.tprocess))),
      seq('insert',alias($.ident,$.fun_call),'(',$._pterm_seq,')',optional(seq(';',$.tprocess))),

      seq('get',alias($.ident,$.fun_call),'(',$._tpattern_seq,')',optional(seq('suchthat',$.pterm)),optional($.options)),
      prec(PREC.IN,seq('get',alias($.ident,$.fun_call),'(',$._tpattern_seq,')',optional(seq('suchthat',$.pterm)),optional($.options),'in',$.tprocess)),
      prec(PREC.ELSE,seq('get',alias($.ident,$.fun_call),'(',$._tpattern_seq,')',optional(seq('suchthat',$.pterm)),optional($.options),'in',$.tprocess,'else',$.tprocess)),

      prec.right(PREC.MID,seq($.tprocess,'|',$.tprocess)),
      seq($.phase,optional(seq(';',$.tprocess))),
      seq('sync',$.int,optional(seq('[',$.ident,']')),optional(seq(';',$.tprocess)))
    )),

    // Basic elements

    // ident: $ => /[a-z]+/,
    ident: $ => choice(
      /[a-zA-Z][a-zA-Z_0-9']*/,
      /~[a-zA-Z_0-9']*/
    ),
    _ident_seq: $ => prec.right(seq($.ident,optional(seq(',',$._ident_seq)))),

    string: $ => /\"[a-zA-Z0-9]+\"/,
    _string_seq: $ => prec.right(seq($.string,optional(seq(',',$._string_seq)))),

    int: $ => /[0-9]+/,
    tag: $ => /[a-z]+/,
    projection: $ => /([ '0'-'9' ])+ "-proj-" [ 'a'-'z' 'A'-'Z' '0'-'9' ] (( [ 'a'-'z' 'A'-'Z' '_' '\\192'-'\\214' '\\216'-'\\246' '\\248'-'\\255' '\\'' '0'-'9' '-' ] )*)/,

    // Comments
    comment: $ => token(
      seq(
        '(*',
        /[^*]*\*+([^)*][^*]*\*+)*/,
        ')'
      )
    ),
  }
});
